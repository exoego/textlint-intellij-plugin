# textlint plugin for JetBrains IDE (Unofficial)
This plugin is an unofficial textlint plugin that displays textlint inspection results in JetBrains IDE (e.g. IntelliJ IDEA).


## Features
* Display textlint issues in IDE.
* Support textlint quickfix.


## Getting started
1. Setup [textlint](https://github.com/textlint/textlint) into your project.
2. Install [this plugin](https://plugins.jetbrains.com/plugin/22788-textlint) from marketplace.
3. Reopen your project.


## LICENSE
[MIT License](./LICENSE)
