package jp.masakura.intellij.annotation.issue

import jp.masakura.intellij.annotation.label.AnnotatorLabel

data class IssueMessage(private val value: String) {
    override fun toString(): String {
        return value
    }

    fun appendLabel(label: AnnotatorLabel): IssueMessage {
        return IssueMessage(label.appendTo(value))
    }

    companion object {
        fun create(message: String, ruleId: String): IssueMessage {
            return IssueMessage("$message ($ruleId)")
        }
    }
}
