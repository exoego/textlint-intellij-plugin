package jp.masakura.intellij.annotation.quickfix

import com.intellij.openapi.editor.Document
import jp.masakura.intellij.annotation.target.Target

class FixCurrentFile(private val fixer: AnnotationFixer, private val target: Target) : Fix {
    override fun fix(document: Document) {
        document.replaceString(0, document.textLength, fixer.fix(target))
    }
}
