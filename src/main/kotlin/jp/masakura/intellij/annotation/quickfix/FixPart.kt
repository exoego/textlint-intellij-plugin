package jp.masakura.intellij.annotation.quickfix

import com.intellij.openapi.editor.Document

data class FixPart(val replaceText: String, val start: Int, val end: Int) : Fix {
    override fun fix(document: Document) {
        document.replaceString(start, end, replaceText)
    }
}
