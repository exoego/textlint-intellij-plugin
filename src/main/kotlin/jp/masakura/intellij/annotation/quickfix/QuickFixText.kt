package jp.masakura.intellij.annotation.quickfix

import jp.masakura.intellij.annotation.label.AnnotatorLabel

data class QuickFixText(private val value: String) {
    override fun toString(): String {
        return value
    }

    fun appendLabel(label: AnnotatorLabel): QuickFixText {
        return QuickFixText(label.appendTo(value))
    }

    companion object {
        fun forPart(ruleId: String): QuickFixText {
            return QuickFixText("Fix '$ruleId'")
        }

        fun forCurrentFile(): QuickFixText {
            return QuickFixText("Fix current file")
        }
    }
}
