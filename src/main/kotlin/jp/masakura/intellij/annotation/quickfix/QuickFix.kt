package jp.masakura.intellij.annotation.quickfix

import com.intellij.openapi.editor.Document
import jp.masakura.intellij.annotation.label.AnnotatorLabel
import jp.masakura.intellij.annotation.target.Target

data class QuickFix(val fix: Fix, val text: QuickFixText) {
    constructor(fix: Fix, text: String) : this(fix, QuickFixText(text))

    fun fix(document: Document) {
        fix.fix(document)
    }

    fun appendLabel(label: AnnotatorLabel): QuickFix {
        return QuickFix(fix, text.appendLabel(label))
    }

    companion object {
        fun forCurrentFile(fixer: AnnotationFixer, target: Target): QuickFix {
            return QuickFix(FixCurrentFile(fixer, target), QuickFixText.forCurrentFile())
        }

        fun forPart(fix: FixPart, ruleId: String): QuickFix {
            return QuickFix(fix, QuickFixText.forPart(ruleId))
        }
    }
}
