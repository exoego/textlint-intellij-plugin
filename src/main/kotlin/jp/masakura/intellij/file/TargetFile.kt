package jp.masakura.intellij.file

import com.intellij.openapi.fileTypes.FileType
import com.intellij.openapi.fileTypes.FileTypes
import java.nio.file.Path
import kotlin.io.path.extension

class TargetFile(
    val path: String,
    val content: String,
    val fileType: FileType = FileTypes.UNKNOWN,
    val modificationCount: Long = 0,
) {
    val extension: String
        get() = Path.of(path).extension

    fun createTemporaryFile(): TemporaryFile {
        return TemporaryFile.create(path, content)
    }
}
