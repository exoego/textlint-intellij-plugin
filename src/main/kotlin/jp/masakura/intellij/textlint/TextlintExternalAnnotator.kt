package jp.masakura.intellij.textlint

import jp.masakura.intellij.annotation.ExternalAnnotatorBase
import jp.masakura.textlint.process.TextlintFactory

class TextlintExternalAnnotator(
    factory: TextlintFactory = TextlintFactory.DEFAULT,
) :
    ExternalAnnotatorBase(TextlintScannerAdapter(factory), TextlintFixerAdapter(factory), TextlintLabel.instance)
