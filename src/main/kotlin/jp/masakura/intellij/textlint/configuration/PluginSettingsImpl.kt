package jp.masakura.intellij.textlint.configuration

import jp.masakura.textlint.process.TextlintExcludeFiles

data class PluginSettingsImpl(override var excludeFiles: TextlintExcludeFiles) : PluginSettings {
    companion object {
        fun default(): PluginSettingsImpl =
            PluginSettingsImpl(TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE)
    }
}
