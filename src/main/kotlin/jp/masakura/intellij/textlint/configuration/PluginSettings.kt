package jp.masakura.intellij.textlint.configuration

import com.intellij.openapi.project.Project
import jp.masakura.intellij.annotation.target.Target
import jp.masakura.textlint.process.TextlintExcludeFiles
import jp.masakura.textlint.process.TextlintSettings

interface PluginSettings {
    var excludeFiles: TextlintExcludeFiles

    fun apply(settings: PluginSettings) {
        apply(settings.toTextlintSettings())
    }

    fun apply(settings: TextlintSettings) {
        excludeFiles = settings.excludeFiles
    }

    fun isModified(original: PluginSettings): Boolean {
        return this.toTextlintSettings() != original.toTextlintSettings()
    }

    fun toTextlintSettings(): TextlintSettings {
        return TextlintSettings(excludeFiles)
    }
}

fun Project.pluginSettings(): PluginSettings {
    return PluginSettingsState.byProject(this)
}

fun Target.textlintSettings(): TextlintSettings {
    return this.project.pluginSettings().toTextlintSettings()
}
