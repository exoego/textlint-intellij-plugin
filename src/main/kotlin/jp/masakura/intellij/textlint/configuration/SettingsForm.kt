package jp.masakura.intellij.textlint.configuration

import jp.masakura.textlint.process.TextlintExcludeFiles
import javax.swing.JPanel
import javax.swing.JRadioButton

class SettingsForm : PluginSettings {
    lateinit var root: JPanel
    private lateinit var dontUseTextlintIgnoreRadioButton: JRadioButton
    private lateinit var useTextlintIgnoreRadioButton: JRadioButton

    override var excludeFiles: TextlintExcludeFiles
        get() {
            if (useTextlintIgnoreRadioButton.isSelected) return TextlintExcludeFiles.USE_TEXTLINT_IGNORE
            return TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE
        }
        set(value) {
            excludeFilesRadioButton(value).isSelected = true
        }

    private fun excludeFilesRadioButton(value: TextlintExcludeFiles): JRadioButton {
        return when (value) {
            TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE -> dontUseTextlintIgnoreRadioButton
            TextlintExcludeFiles.USE_TEXTLINT_IGNORE -> useTextlintIgnoreRadioButton
        }
    }
}
