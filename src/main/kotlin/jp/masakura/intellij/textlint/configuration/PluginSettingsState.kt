package jp.masakura.intellij.textlint.configuration

import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.project.Project
import jp.masakura.textlint.process.TextlintExcludeFiles
import jp.masakura.textlint.process.TextlintSettings

@State(
    name = "jp.masakura.intellij.textlint.configuration.PluginSettingsState",
    storages = [Storage("textlint.xml")],
)
@Service(Service.Level.PROJECT)
class PluginSettingsState : PersistentStateComponent<PluginSettingsState>, PluginSettings {
    override var excludeFiles: TextlintExcludeFiles = TextlintSettings.DEFAULT.excludeFiles

    override fun getState(): PluginSettingsState {
        return this
    }

    override fun loadState(state: PluginSettingsState) {
        this.apply(state)
    }

    companion object {
        fun byProject(project: Project): PluginSettingsState {
            return project.getService(PluginSettingsState::class.java)
        }

        fun from(settings: TextlintSettings): PluginSettingsState {
            val state = PluginSettingsState()
            state.apply(settings)
            return state
        }
    }
}
