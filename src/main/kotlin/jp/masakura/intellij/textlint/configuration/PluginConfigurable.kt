package jp.masakura.intellij.textlint.configuration

import com.intellij.openapi.options.Configurable
import com.intellij.openapi.project.Project
import javax.swing.JComponent

class PluginConfigurable(private val project: Project) : Configurable {
    private val form by lazy {
        SettingsForm()
    }

    private val state: PluginSettings
        get() = project.pluginSettings()

    override fun createComponent(): JComponent {
        return form.root
    }

    override fun isModified(): Boolean {
        return form.isModified(state)
    }

    override fun apply() {
        state.apply(form)
    }

    override fun reset() {
        form.apply(state)
    }

    @Suppress("DialogTitleCapitalization")
    override fun getDisplayName(): String = "textlint"
}
