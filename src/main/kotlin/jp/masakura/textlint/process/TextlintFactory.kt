package jp.masakura.textlint.process

interface TextlintFactory {
    fun create(workDirectory: String, settings: TextlintSettings): Textlint

    companion object {
        fun withCache(factory: TextlintFactory): TextlintFactory {
            return CachedTextlintFactory(factory)
        }

        val DEFAULT: TextlintFactory = withCache(TextlintFactoryImpl())
    }
}
