package jp.masakura.textlint.process

import jp.masakura.intellij.file.TargetFile
import jp.masakura.textlint.command.TextlintCommand
import jp.masakura.textlint.configuration.PrintConfig
import jp.masakura.textlint.report.TextlintReport

class TextlintImpl(
    private val workDirectory: String,
    private val textlint: TextlintCommand,
) : Textlint {
    private val printConfig by lazy {
        PrintConfig.create(textlint)
    }

    override fun scan(file: TargetFile): TextlintReport {
        val process = execute(listOf("--format", "json", "--stdin", "--stdin-filename", file.path))

        process.outputWriter(Charsets.UTF_8).use {
            it.appendLine(file.content)
        }

        return TextlintReport.from(process.inputStream)
    }

    override fun fix(file: TargetFile): String {
        file.createTemporaryFile().use {
            val process = execute(listOf("--fix", it.absolutePath))

            process.waitFor()

            return it.load()
        }
    }

    override fun isSupported(file: TargetFile): Boolean {
        return printConfig.get().isSupported(file)
    }

    private fun execute(parameters: List<String>): Process {
        return textlint.execute(parameters)
    }

    override fun toString(): String {
        return """TextlintImpl("$workDirectory")"""
    }
}
