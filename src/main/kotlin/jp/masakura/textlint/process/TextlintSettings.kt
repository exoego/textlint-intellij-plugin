package jp.masakura.textlint.process

import jp.masakura.textlint.command.TextlintCommand
import jp.masakura.textlint.file.FileSystem

data class TextlintSettings(val excludeFiles: TextlintExcludeFiles) {
    fun createTextlint(workDirectory: String, command: TextlintCommand, files: FileSystem): Textlint {
        return when (excludeFiles) {
            TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE -> Textlint.dontUseTextlintIgnore(workDirectory, command, files)
            TextlintExcludeFiles.USE_TEXTLINT_IGNORE -> Textlint.useTextlintIgnore(workDirectory, command, files)
        }
    }

    companion object {
        val DEFAULT: TextlintSettings = TextlintSettings(TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE)
    }
}
