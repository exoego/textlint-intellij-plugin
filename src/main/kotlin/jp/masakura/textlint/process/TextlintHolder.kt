package jp.masakura.textlint.process

class TextlintHolder(
    private val settings: TextlintSettings,
    val textlint: Textlint,
) {
    fun isSameSetting(settings: TextlintSettings): Boolean {
        return this.settings == settings
    }
}
