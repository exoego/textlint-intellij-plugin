package jp.masakura.textlint.process

import jp.masakura.intellij.file.TargetFile
import jp.masakura.textlint.command.TextlintCommand
import jp.masakura.textlint.file.FileSystem
import jp.masakura.textlint.report.TextlintReport

interface Textlint {
    fun scan(file: TargetFile): TextlintReport
    fun fix(file: TargetFile): String
    fun isSupported(file: TargetFile): Boolean

    companion object {
        fun dontUseTextlintIgnore(workDirectory: String, command: TextlintCommand, files: FileSystem): Textlint {
            return TextlintrcTextlint(
                TextlintImpl(workDirectory, command),
                workDirectory,
                files,
            )
        }

        fun useTextlintIgnore(workDirectory: String, command: TextlintCommand, files: FileSystem): Textlint {
            return TextlintrcTextlint(
                TextlintIgnoreTextlint(
                    TextlintImpl(workDirectory, command),
                    workDirectory,
                    files,
                ),
                workDirectory,
                files,
            )
        }
    }
}
