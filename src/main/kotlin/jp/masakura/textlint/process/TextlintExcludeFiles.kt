package jp.masakura.textlint.process

enum class TextlintExcludeFiles {
    DONT_USE_TEXTLINT_IGNORE,
    USE_TEXTLINT_IGNORE,
}
