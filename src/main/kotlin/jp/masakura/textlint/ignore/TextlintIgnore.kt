package jp.masakura.textlint.ignore

import jp.masakura.textlint.file.FileSystem
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.relativeTo

class TextlintIgnore(
    private val matcher: GlobMatcher,
    private val workDirectory: Path,
) {
    constructor(matcher: GlobMatcher, workDirectory: String) :
        this(matcher, Path.of(workDirectory))

    fun isIgnore(absolutePath: String): Boolean {
        return isIgnore(Path.of(absolutePath))
    }

    private fun isIgnore(absolutePath: Path): Boolean {
        val relativePath = absolutePath.relativeTo(workDirectory)
        return matcher.matches(relativePath)
    }

    companion object {
        fun parse(text: String, workDirectory: String): TextlintIgnore {
            val patterns = TextlintIgnoreParser.INSTANCE.parse(text)
            val matchers = GlobMatcher.createAll(patterns)
            return TextlintIgnore(matchers, workDirectory)
        }

        fun load(workDirectory: String, files: FileSystem): TextlintIgnore {
            val ignorePath = Paths.get(workDirectory, ".textlintignore").toString()
            if (!files.exists(ignorePath)) return TextlintIgnore(GlobMatcher.EMPTY, workDirectory)

            return parse(files.readText(ignorePath), workDirectory)
        }
    }
}
