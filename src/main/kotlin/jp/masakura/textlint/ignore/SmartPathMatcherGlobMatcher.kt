package jp.masakura.textlint.ignore

/**
 * GlobMatcher, which is attempting to align the behavior of PathMatcher as closely as possible with the glob pattern of .textlintignore.
 *
 * * The .textlintignore matches file.txt with `**_slash_*.txt`, but PathMatcher doesn't. GlobMatcher aims to resolve this discrepancy.
 */
class SmartPathMatcherGlobMatcher {
    companion object {
        fun create(pattern: String): GlobMatcher {
            val matchers = adjust(pattern).map { PathMatcherGlobMatcher.create(it) }
            return GlobMatchers(matchers)
        }

        private fun adjust(pattern: String): List<String> {
            // if pattern is `**/*.txt`

            // * `**/*.txt`
            return listOf(pattern) +
                // * `*.txt`
                GlobstarPatternAdjuster.adjust(pattern)
        }
    }
}
