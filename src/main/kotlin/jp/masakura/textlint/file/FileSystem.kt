package jp.masakura.textlint.file

interface FileSystem {
    fun exists(absolutePath: String): Boolean

    fun readText(absolutePath: String): String

    companion object {
        val INSTANCE: FileSystem = FileSystemImpl()
    }
}
