package jp.masakura.textlint.configuration

import jp.masakura.intellij.file.TargetFile
import jp.masakura.textlint.configuration.element.Configuration
import jp.masakura.textlint.configuration.processor.Processor
import java.io.InputStream

data class PrintedConfig(private val configuration: Configuration) {
    fun isSupported(file: TargetFile): Boolean {
        val processors = Processor.get(configuration.plugin.map { it.id })
        return processors.isSupported(file.extension)
    }

    companion object {
        fun parse(input: InputStream): PrintedConfig {
            return PrintedConfig(Configuration.parse(input))
        }
    }
}
