package jp.masakura.textlint.configuration.element

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.io.InputStream

data class Configuration(val plugin: List<Plugin> = emptyList()) {
    companion object {
        fun parse(input: InputStream): Configuration {
            return serializer.readValue<Configuration>(input)
        }

        private val serializer = jacksonObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    }
}
