package jp.masakura.textlint.configuration

import jp.masakura.textlint.command.TextlintCommand

class PrintConfigImpl(private val textlint: TextlintCommand) :
    PrintConfig {
    override fun get(): PrintedConfig {
        val process = textlint.execute(listOf("--print-config"))
        process.waitFor()

        return PrintedConfig.parse(process.inputStream)
    }
}
