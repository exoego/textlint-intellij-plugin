package jp.masakura.textlint.report

data class Location(val start: Position, val end: Position)
