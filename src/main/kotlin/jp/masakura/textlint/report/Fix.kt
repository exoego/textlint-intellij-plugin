package jp.masakura.textlint.report

data class Fix(val range: List<Int>, val text: String)
