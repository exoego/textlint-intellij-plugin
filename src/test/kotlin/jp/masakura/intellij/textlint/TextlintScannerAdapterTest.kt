package jp.masakura.intellij.textlint

import jp.masakura.testing.file.FakeTextlintFactory
import jp.masakura.testing.fixture.Fixture
import jp.masakura.testing.fixture.Fixtures
import jp.masakura.testing.project.FakeProject
import jp.masakura.textlint.process.TextlintExcludeFiles
import jp.masakura.textlint.process.TextlintSettings
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class TextlintScannerAdapterTest {
    private lateinit var project: FakeProject
    private lateinit var fixture: Fixture
    private lateinit var target: TextlintScannerAdapter

    @BeforeEach
    fun setUp() {
        project = FakeProject.create("/project1", TextlintSettings(TextlintExcludeFiles.USE_TEXTLINT_IGNORE))

        fixture = Fixtures.file
        val factory = FakeTextlintFactory()
            .printedConfig(Fixtures.printedConfigs.DEFAULT)
            .add(fixture)
        target = TextlintScannerAdapter(factory)
    }

    @Test
    fun testScan() {
        val t = project.target("file.txt", fixture.file.content)

        assertEquals(fixture.annotations, target.scan(t))
    }
}
