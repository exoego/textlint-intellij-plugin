package jp.masakura.intellij.textlint.configuration

import jp.masakura.textlint.process.TextlintExcludeFiles
import jp.masakura.textlint.process.TextlintSettings
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class PluginSettingsTest {
    @Test
    fun testApply() {
        val source = PluginSettingsImpl(TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE)
        val destination = PluginSettingsImpl(TextlintExcludeFiles.USE_TEXTLINT_IGNORE)

        source.apply(destination)

        assertEquals(destination, source)
    }

    @Test
    fun modified() {
        val original = PluginSettingsImpl(TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE)
        val current = PluginSettingsImpl(TextlintExcludeFiles.USE_TEXTLINT_IGNORE)

        assertTrue(current.isModified(original))
    }

    @Test
    fun `not modified`() {
        val original = PluginSettingsImpl(TextlintExcludeFiles.USE_TEXTLINT_IGNORE)
        val current = PluginSettingsImpl(TextlintExcludeFiles.USE_TEXTLINT_IGNORE)

        assertFalse(current.isModified(original))
    }

    @Test
    fun testToTextlintSettings() {
        val source = PluginSettingsImpl(TextlintExcludeFiles.USE_TEXTLINT_IGNORE)

        assertEquals(
            TextlintSettings(TextlintExcludeFiles.USE_TEXTLINT_IGNORE),
            source.toTextlintSettings(),
        )
    }
}
