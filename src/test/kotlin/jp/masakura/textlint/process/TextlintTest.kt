package jp.masakura.textlint.process

import jp.masakura.intellij.file.TargetFile
import jp.masakura.intellij.file.TemporaryFile
import jp.masakura.testing.file.FakeTextlintFactory
import jp.masakura.testing.fixture.Fixtures
import org.junit.jupiter.api.Assertions.* // ktlint-disable no-wildcard-imports
import org.junit.jupiter.api.Test

class TextlintTest {
    @Test
    fun testScan() {
        val fixture = Fixtures.issue
        val target = FakeTextlintFactory()
            .printedConfig(Fixtures.printedConfigs.DEFAULT)
            .add(fixture)
            .createUseTextlintIgnore("/project1")

        val actual = target.scan(TargetFile("sample.md", fixture.file.content))

        assertEquals(fixture.textlint.report, actual)
    }

    @Test
    fun testFix() {
        val fixture = Fixtures.fix
        val target = FakeTextlintFactory()
            .printedConfig(Fixtures.printedConfigs.DEFAULT)
            .add(fixture)
            .createUseTextlintIgnore("/project1")

        val actual = target.fix(TargetFile("sample.md", fixture.file.content))

        assertEquals("ポケット エンジン", actual.trim())
    }

    @Test
    fun `The original file's content does not change with a fix`() {
        val target = FakeTextlintFactory()
            .createUseTextlintIgnore("/project1")

        TemporaryFile.create("sample.md", "ポケット エンジン").use {
            val file = it.targetFile()
            val originalContent = it.load()

            target.fix(file)

            assertEquals(originalContent, it.load())
        }
    }

    @Test
    fun `textlintrc file not exists, not all file types are supported`() {
        val target = FakeTextlintFactory()
            .createUseTextlintIgnore("/project1")

        assertFalse(target.isSupported(emptyFile("/project1/sample.txt")))
    }

    @Test
    fun `textlintrc file exists, markdown and plaintext are supported`() {
        val target = FakeTextlintFactory()
            .printedConfig(Fixtures.printedConfigs.DEFAULT)
            .createUseTextlintIgnore("/project1")

        assertTrue(target.isSupported(emptyFile("/project1/sample.md")))
    }

    @Test
    fun `files specified in textlintignore are ignored`() {
        val target = FakeTextlintFactory()
            .printedConfig(Fixtures.printedConfigs.DEFAULT)
            .textlintIgnore("file.txt")
            .createUseTextlintIgnore("/project1")

        assertFalse(target.isSupported(emptyFile("/project1/file.txt")))
    }

    private fun emptyFile(absolutePath: String): TargetFile {
        return TargetFile(absolutePath, "")
    }
}
