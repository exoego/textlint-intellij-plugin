package jp.masakura.textlint.process

import jp.masakura.textlint.command.TextlintCommand
import jp.masakura.textlint.file.FileSystem
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.mock

class TextlintSettingsTest {
    private lateinit var files: FileSystem
    private lateinit var command: TextlintCommand

    @BeforeEach
    fun setUp() {
        command = mock<TextlintCommand>()
        files = mock<FileSystem>()
    }

    @Test
    fun testCreateTextlintDontUseTextlintIgnore() {
        val target = TextlintSettings(TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE)

        Assertions.assertEquals(
            """TextlintrcTextlint -> TextlintImpl("/project1")""",
            target.createTextlint("/project1").toString(),
        )
    }

    @Test
    fun testCreateTextlintUseTextlintIgnore() {
        val target = TextlintSettings(TextlintExcludeFiles.USE_TEXTLINT_IGNORE)

        Assertions.assertEquals(
            """TextlintrcTextlint -> TextlintIgnoreTextlint -> TextlintImpl("/project1")""",
            target.createTextlint("/project1").toString(),
        )
    }

    private fun TextlintSettings.createTextlint(workDirectory: String): Textlint {
        return this.createTextlint(workDirectory, command, files)
    }
}
