package jp.masakura.textlint.configuration

import jp.masakura.textlint.configuration.element.Configuration
import jp.masakura.textlint.configuration.element.Plugin
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertSame
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.mock

class CachedPrintConfigTest {
    private lateinit var target: PrintConfig
    private lateinit var mockPrintConfig: PrintConfig

    @BeforeEach
    fun setUp() {
        mockPrintConfig = mock<PrintConfig> {
            on { get() } doAnswer { config() }
        }

        target = PrintConfig.withCache(mockPrintConfig)
    }

    @Test
    fun `start by loading the settings`() {
        assertEquals(config(), target.get())
    }

    @Test
    fun `return the cache the second time`() {
        val first = target.get() // add to cache

        assertSame(first, target.get())
    }

    companion object {
        private fun config(): PrintedConfig {
            return PrintedConfig(
                Configuration(
                    listOf(
                        Plugin("html"),
                    ),
                ),
            )
        }
    }
}
