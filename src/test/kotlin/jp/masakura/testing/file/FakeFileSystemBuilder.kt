package jp.masakura.testing.file

import jp.masakura.textlint.file.FileSystem

class FakeFileSystemBuilder(private val files: List<FakeFile> = emptyList()) {
    fun add(relativePath: String): FakeFileSystemBuilder {
        return add(FakeFile.empty(relativePath))
    }

    fun add(file: FakeFile): FakeFileSystemBuilder {
        return FakeFileSystemBuilder(files + listOf(file))
    }

    fun build(workDirectory: String): FileSystem {
        return FakeFileSystem(files.map { it.toAbsolutePathFile(workDirectory) })
    }
}
