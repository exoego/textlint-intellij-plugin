package jp.masakura.testing.textlint

import jp.masakura.testing.fixture.Fixture
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream

class FakeTextlintScanProcess private constructor(
    private val fixtures: List<Fixture>,
) : Process() {
    private val output = ByteArrayOutputStream()

    override fun getOutputStream(): OutputStream {
        return output
    }

    override fun getInputStream(): InputStream {
        val content = String(output.toByteArray(), Charsets.UTF_8)
        val fixture = fixtures.find { it.file.content.trim() == content.trim() }
        return fixture!!.textlint.json.byteInputStream(Charsets.UTF_8)
    }

    override fun getErrorStream(): InputStream {
        return "".byteInputStream(Charsets.UTF_8)
    }

    override fun waitFor(): Int {
        return exitValue()
    }

    override fun exitValue(): Int {
        return 0
    }

    override fun destroy() {
    }

    companion object {
        fun create(options: TextlintCommandLineOptions, fixtures: List<Fixture>): Process {
            if (options.format != "json") throw IllegalStateException("require --format is 'json'")

            if (options.stdin) {
                return FakeTextlintScanProcess(
                    fixtures,
                )
            }

            throw IllegalStateException("unknown execution type")
        }
    }
}
