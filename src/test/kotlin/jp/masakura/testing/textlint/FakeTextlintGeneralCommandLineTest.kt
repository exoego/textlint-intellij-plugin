package jp.masakura.testing.textlint

import jp.masakura.testing.fixture.Fixtures
import jp.masakura.textlint.command.Npx
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.io.path.Path

class FakeTextlintGeneralCommandLineTest {
    @Test
    fun test() {
        val workDirectory = Path(System.getProperty("user.dir")).resolve("test-project")
        val file = Path("directory").resolve("file.md")

        val textlint = FakeTextlintGeneralCommandLineBuilder()
            .add(Fixtures.file)
            .build()

        val process = textlint
            .withExePath(Npx.DEFAULT.command)
            .withWorkDirectory(workDirectory.toString())
            .withParameters("textlint", "--format", "json", "--stdin", "--stdin-filename", file.toString())
            .createProcess()

        process.outputWriter(Charsets.UTF_8).use {
            it.appendLine(Fixtures.file.file.content)
        }

        process.waitFor()

        val actual = process.inputReader(Charsets.UTF_8).use { it.readText().trim() }

        assertEquals(Fixtures.file.textlint.json, actual)
    }
}
