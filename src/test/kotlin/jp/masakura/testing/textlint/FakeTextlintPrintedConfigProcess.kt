package jp.masakura.testing.textlint

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.io.OutputStream
import java.nio.charset.StandardCharsets

class FakeTextlintPrintedConfigProcess private constructor(
    private val textlintrc: String,
) : Process() {
    companion object {
        fun create(options: TextlintCommandLineOptions, textlintrc: String): Process? {
            if (options.printConfig) return FakeTextlintPrintedConfigProcess(textlintrc)
            return null
        }
    }

    override fun getOutputStream(): OutputStream {
        throw IllegalStateException("Not supported")
    }

    override fun getInputStream(): InputStream {
        return ByteArrayInputStream(textlintrc.toByteArray(StandardCharsets.UTF_8))
    }

    override fun getErrorStream(): InputStream {
        throw IllegalStateException("Not supported")
    }

    override fun waitFor(): Int {
        return 0
    }

    override fun exitValue(): Int {
        throw IllegalStateException("Not supported")
    }

    override fun destroy() {
        throw IllegalStateException("Not supported")
    }
}
