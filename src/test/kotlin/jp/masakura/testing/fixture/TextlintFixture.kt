package jp.masakura.testing.fixture

import jp.masakura.textlint.report.TextlintReport

data class TextlintFixture(
    val json: String,
    val report: TextlintReport,
)
