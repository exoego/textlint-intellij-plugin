# CHANGELOG
## [0.7.0](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/releases/0.7.0) - 2023-??-??
- [#57](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/issues/57) - annotation caching
- [#56](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/issues/56) - modify to utilize .textlintignor (low compatibility)
- [#64](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/issues/64)- add setting gui
- [#67](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/issues/67) - fix bug that sometimes throws exceptions even when not in use


## [0.6.0](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/releases/0.6.0) - 2023-10-17
- [#54](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/issues/54) - fix IntelliJ 2023.2.2 compatibility warnings
- [#59](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/issues/59) - fix a bug that took a long time to annotate (Windows)


## [0.5.0](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/releases/0.5.0) - 2023-10-11
- [#35](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/issues/35) - support html
- [#45](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/issues/45) - support rst


## [0.4.0](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/releases/0.4.0) - 2023-10-07
- [#24](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/issues/24) - add fix current file


## [0.3.0](https://gitlab.com/jetbrains-ide-plugins/textlint-intellij-plugin/-/releases/0.3.0) - 2023-10-04
Initial release.
